package ExtentReport;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import pages.CerotidPageObjectsAndMethods;

public class ExtentReportsDemoWithTestNg {
	// Global variable
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	WebDriver driver;

	@BeforeSuite // -> Runs only Once in the case we have more then one @com.cerotid.qa.test annotaion
	public void setUp() {

		// Creating ExtenReporter Object and creating new extentReport Html file
		// Create ExtentReports and attach reporter(s)
		htmlReporter = new ExtentHtmlReporter("cerotidExtentreport.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
	}

	// @BeforeTest --> Runs everytime setup after every @com.cerotid.qa.test annoations
	@BeforeTest
	public void setUpTestInvokeBrowser() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();

	}

	public void navigateToWebPage() {
		driver.get("http://www.cerotid.com");
		if (driver.getTitle().contains("Cerotid")) {
			System.out.println(driver.getTitle() + "----------------------------------------------------Was lauched");
		} else {
			System.out.println("Failed--------------------Incorrect title page");
			System.exit(0);
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		ExtentTest isBrowserOpened = extent.createTest("Invoke Browser",
				"This com.cerotid.qa.test insures that the browser is Invoked");
		isBrowserOpened.pass("Browser was Invoked as Expected");
	}

	@Test
	public void CerotidHomePageTest1() throws Exception {
		// Navigate to cerotid page
		navigateToWebPage();

		// Creates a toggle for the given com.cerotid.qa.test, add all logs events under it
		ExtentTest test = extent.createTest("Cerotid Register for training course",
				"This Test validates the functionality to Register for training");
		test.log(Status.INFO, "Executing com.cerotid.qa.test case");

		// Creating Page object
		CerotidPageObjectsAndMethods cerotidPageObj = new CerotidPageObjectsAndMethods(driver);
		// Utlizing Cerotid page objects and performing actions
		// Sending Validations to Extent reports
		cerotidPageObj.selectCourse("QA Automation");
		test.pass("Selected Course");
		cerotidPageObj.selectSession("Upcoming Session");
		test.pass("Selected Session");
		cerotidPageObj.enterName("Test Tester");
		test.pass("Entered Name");
		cerotidPageObj.enterAddress("2300 Valley View Ln Suite 620, Irving, TX 75062");
		test.pass("Entered Address");
		cerotidPageObj.enterCity("Irving");
		test.pass("Entered City");
		cerotidPageObj.selectState("TX");
		test.pass("Selected State");
		cerotidPageObj.enterZip("75062");
		test.pass("Entered zip");
		cerotidPageObj.enterEmail("com.cerotid.qa.test@cerotid.com");
		test.pass("Enetered Email");
		cerotidPageObj.enterPhoneNumber("(972)255-0046");
		test.pass("Entered Phone Number");
		cerotidPageObj.chooseVisa("Other");
		test.pass("Selected Visa status");
		cerotidPageObj.chooseMediaSource("Social Media");
		test.pass("Selected Social Media");
		cerotidPageObj.relocateBtn();
		test.pass("Click on Yes button");
		cerotidPageObj.eduBgForm("Currently Learning at Cerotid");
		test.pass("Enter Background Info");

		// com.cerotid.qa.test with snapshot
		test.addScreenCaptureFromPath("screenshot.png");

		// Ending extent com.cerotid.qa.test case
		test.log(Status.INFO, "Ending com.cerotid.qa.test case");

	}
	
	public boolean firstLast6(int[] nums) {
		boolean check = false;
		
		if((nums[0] == 6) || (nums[nums.length - 1] == 6)) {
			check = true;
		}else {
			check =  false; 
		}
		
		
		
		
		return check;
		
	}

	// @Test(enabled =false) --> disables a com.cerotid.qa.test
	@Test
	public void CerotidHomePageTest2() throws Exception {

		ExtentTest test2 = extent.createTest("Cerotid Negative Test",
				"This Test is failed for negative testing purposes");
		test2.log(Status.INFO, "Executing com.cerotid.qa.test case");
		// log with snapshot
		test2.fail("Test was failed on purpose for negative testing",
				MediaEntityBuilder.createScreenCaptureFromPath("screenshot.png").build());

	}
	
//	@Test
//	public void navigateToSecondWebsite() {
//		
//	}

	// @AfterTest Runs everytime after @com.cerotid.qa.test's
	@AfterTest
	public void tearDownTestCloseBrowser() {
		// close browser
		driver.close();
		driver.quit();
		ExtentTest isBrowserClosed = extent.createTest("Terminate Browser",
				"This com.cerotid.qa.test insures that the browser is terminated");
		isBrowserClosed.pass("Browser is Terminated as Expected");
	}

	// -> runs only once after all @tests
	@AfterSuite
	public void tearDown() {
		// caling the flush writes everything to the log file
		extent.flush();

	}

}
