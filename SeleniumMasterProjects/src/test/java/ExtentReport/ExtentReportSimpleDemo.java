package ExtentReport;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import pages.CerotidPageObjectsAndMethods;

public class ExtentReportSimpleDemo {

	private static WebDriver driver = null;

	public static void main(String[] args) {

		// Creating ExtenReporter Object and creating new extentReport Html file
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extentReports.html");
		// Create ExtentReports and attach reporter(s)
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		// Creates a toggle for the given com.cerotid.qa.test, add all logs events under it
		ExtentTest test = extent.createTest("Cerotid Form Test",
				"This is a com.cerotid.qa.test to validate the functionality of The form section");

		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver\\chromedriver.exe");

		driver = new ChromeDriver();
		// log and info example
		test.log(Status.INFO, "Executing com.cerotid.qa.test case");

		driver.get("http://www.cerotid.com");
		if (driver.getTitle().contains("Cerotid")) {
			// pass example
			test.pass("Navigated to Cerotid Website");
			System.out.println(driver.getTitle() + "----------------------------------------------------Was lauched");
		}

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// -------------------------------------------------------------
		// Creating Page object
		CerotidPageObjectsAndMethods cerotidPageObj = new CerotidPageObjectsAndMethods(driver);
		// Utlizing Cerotid page objects and performing actions
		cerotidPageObj.selectCourse("QA Automation");
		test.pass("Selected Course");

		cerotidPageObj.selectSession("Upcoming Session");
		test.pass("Selected Session");

		cerotidPageObj.enterName("Test Tester");
		test.pass("Entered Name");

		cerotidPageObj.enterAddress("2300 Valley View Ln Suite 620, Irving, TX 75062");
		test.pass("Entered Address");

		cerotidPageObj.enterCity("Irving");
		test.pass("Entered City");

		cerotidPageObj.selectState("TX");
		test.pass("Selected State");

		cerotidPageObj.enterZip("75062");
		test.pass("Entered zip");

		cerotidPageObj.enterEmail("com.cerotid.qa.test@cerotid.com");
		test.pass("Enetered Email");

		cerotidPageObj.enterPhoneNumber("(972)255-0046");
		test.pass("Entered Phone Number");

		cerotidPageObj.chooseVisa("Other");
		test.pass("Selected Visa status");

		cerotidPageObj.chooseMediaSource("Social Media");
		test.pass("Selected Social Media");

		cerotidPageObj.relocateBtn();
		test.pass("Click on Yes button");

		cerotidPageObj.eduBgForm("Currently Learning at Cerotid");
		test.pass("Enter Background Info");

		driver.close();
		driver.quit();
		
		test.log(Status.INFO, "Ending com.cerotid.qa.test case");
		test.pass("Closed Browser");

		// creating flush writes everything to the log file
		extent.flush();

	}

}
