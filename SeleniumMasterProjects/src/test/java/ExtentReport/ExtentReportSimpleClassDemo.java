package ExtentReport;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReportSimpleClassDemo {

	// Creating a webdriver object
	static WebDriver driver;

	// Main Method to run com.cerotid.qa.test
	public static void main(String[] args) {

		// Creating ExtenTrporter Object and creating new extenReport Html File
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("googleExtentReports.html");

		// creating Extentreports and attaching reporter
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		// Create a toggle for the given com.cerotid.qa.test, add logs and events under it
		ExtentTest test = extent.createTest("Google Search Test",
				"This is a tes to validate the functionality of the search box");

		// Setting the System path to utilize the chromedriver
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// Creating a new chromedriver
		driver = new ChromeDriver();

		// Log-info Example
		test.log(Status.INFO, "Executing Test case");

		// Navigating to google.com
		driver.get("http://www.google.com");

		// Validating the Title
		if (driver.getTitle().contains("Google")) {
			// Log-Pass Eaxmple
			test.pass("Google title is displayed as Expected");
		} else {
			// Log-Fail Example
			test.fail("goole title is not displayed as Expected");
		}

		// Maxmizing the Browser
		driver.manage().window().maximize();

		// Add Waites
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// Creating WebElement Objects
		By bytxtBoxSearch = By.xpath("//input[@name='q']");
		By bySearchButton = By.xpath("//input[@name='btnK']");

		WebElement txtBoxSearch = driver.findElement(bytxtBoxSearch);
		WebElement searchButton = driver.findElement(bySearchButton);

		// Creating string variable to hold message
		String messsage = "Valentines Week";
		// Sending keys with message
		txtBoxSearch.sendKeys(messsage);
		WebElement searchBoxValue = driver.findElement(By.xpath("(//div[@class='sbl1']//span/b)[1]"));
		// Validating if keys were sent
		System.out.println(searchBoxValue.getText());
		if (searchBoxValue.getText().contains("valentine")) {
			test.pass("Keys were sent as Expected");
		} else {
			test.fail("Keys were not sent as Expected");
		}

		if (isElementPresent(bySearchButton)) {
			test.pass("Google Search Button is Displayed as Expected");
			searchButton.click();
		} else {
			test.fail("Google Search Button is not Displayed as Expected");
		}

		// Log-Info Example
		test.log(Status.INFO, "Ending com.cerotid.qa.test case");
		driver.close();
		driver.quit();
		test.pass("Closed Browser");

		// creating flush writes everything to the log file
		extent.flush();

	}

	// Returns True or False Depending on if the Element is Present or Not
	public static boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

}
