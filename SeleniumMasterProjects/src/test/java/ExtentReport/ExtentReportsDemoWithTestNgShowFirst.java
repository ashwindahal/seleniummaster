package ExtentReport;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import pages.CerotidPageObjectsAndMethods;

public class ExtentReportsDemoWithTestNgShowFirst {
	// Global variable
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	ExtentTest test = null;
	WebDriver driver;

	// @BeforeTest --> Runs everytime setup after every @com.cerotid.qa.test annoations
	@BeforeSuite // -> Runs only Once in the case we have more then one @com.cerotid.qa.test annotaion
	public void setUp() {

		// Creating ExtenReporter Object and creating new extentReport Html file
		// Create ExtentReports and attach reporter(s)
		htmlReporter = new ExtentHtmlReporter("extent.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
	}

	@BeforeTest
	public void setUpTestInvokeBrowser() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
		test.log(Status.INFO, "Executing com.cerotid.qa.test case");

	}

	public void navigateToWebPage() {
		driver.get("http://www.cerotid.com");
		driver.navigate().refresh();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test
	public void cerotidPageTest1() throws Exception {
		
		navigateToWebPage();

		// Creates a toggle for the given com.cerotid.qa.test, add all logs events under it
		test = extent.createTest("Cerotid Form Test",
				"This Test validates the functionality to to Register for training");

		// info(status, details)
		test.log(Status.INFO, "This step showns usage of log(status, details)");

		// info(details)
		test.info("This step shows usage of info(details)");

		// log with snapshot
		test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath("screenshot.png").build());

		// com.cerotid.qa.test with snapshot
		test.addScreenCaptureFromBase64String("screenshot.png");

	}

	// @Test(enabled =false) --> disables a com.cerotid.qa.test
	@Test
	public void cerotidPageTest2() throws Exception {
		// Creates a toggle for the given com.cerotid.qa.test, add all logs events under it
		ExtentTest test = extent.createTest("Cerotid Form Test",
				"This Test validates the functionality to to Register for training");

		// info(status, details)
		test.log(Status.INFO, "This step showns usage of log(status, details)");

		// info(details)
		test.info("This step shows usage of info(details)");

		test.skip("Skipping");

		// log with snapshot
		test.pass("details", MediaEntityBuilder.createScreenCaptureFromPath("screenshot.png").build());

		// com.cerotid.qa.test with snapshot
		test.addScreenCaptureFromBase64String("screenshot.png");

	}

	@AfterTest
	public void tearDownTestCloseBrowser() {
		// close browser
		driver.close();
		driver.quit();
		test.log(Status.INFO, "Ending com.cerotid.qa.test case");
		test.pass("Closed Browser");
	}

	// @AfterTest runs everytime after @com.cerotid.qa.test's
	@AfterSuite // -> runs only once after all @tests
	public void tearDown() {
		// caling the flush writes everything to the log file
		extent.flush();

	}

}
