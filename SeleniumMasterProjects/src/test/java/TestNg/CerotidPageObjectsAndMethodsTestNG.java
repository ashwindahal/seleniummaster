package TestNg;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.CerotidPageObjectsAndMethods;

public class CerotidPageObjectsAndMethodsTestNG {

	WebDriver driver = null;

	@BeforeTest
	public void setUpTestInvokeBrowser() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();

	}

	public void navigateToWebPage() {
		driver.get("http://www.cerotid.com");
		driver.navigate().refresh();
		System.out.println(driver.getTitle() + "----------------------------------------------------Was lauched");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test
	public void cerotidSignUpFlow() throws InterruptedException {
		// Invoke browser
		navigateToWebPage();

		// Creating Page object
		CerotidPageObjectsAndMethods cerotidPageObj = new CerotidPageObjectsAndMethods(driver);

		// Utlizing Cerotid page objects and performing actions
		cerotidPageObj.selectCourse("QA Automation");
		cerotidPageObj.selectSession("Upcoming Session");
		cerotidPageObj.enterName("Test Tester");
		cerotidPageObj.enterAddress("2300 Valley View Ln Suite 620, Irving, TX 75062");
		cerotidPageObj.enterCity("Irving");
		cerotidPageObj.selectState("TX");
		cerotidPageObj.enterZip("75062");
		cerotidPageObj.enterEmail("com.cerotid.qa.test@cerotid.com");
		cerotidPageObj.enterPhoneNumber("(972)255-0046");
		cerotidPageObj.chooseVisa("Other");
		cerotidPageObj.chooseMediaSource("Social Media");
		cerotidPageObj.relocateBtn();
		cerotidPageObj.eduBgForm("Currently Learning at Cerotid");
		cerotidPageObj.clickOnSubmit();

		// Validate Message
		validateSuccessMessage();
	}

	public void validateSuccessMessage() {
		boolean isSuucessMessageDisplayed = false;
		String expectedMessage = "Your register is completed. We will contact you shortly!";

		CerotidPageObjectsAndMethods cerotidPageObj = new CerotidPageObjectsAndMethods(driver);
		String messageFromUI = cerotidPageObj.validateMessage();

		if (messageFromUI.equalsIgnoreCase(expectedMessage)) {
			isSuucessMessageDisplayed = true;
		}

		if (isSuucessMessageDisplayed) {
			System.out.println(
					"Pass- Expected Message 'Your register is completed. We will contact you shortly!' is displayed as Expected");
		} else {
			System.out.println(
					"Expected Message 'Your register is completed. We will contact you shortly!' is not displayed as Expected");
		}

	}

	@AfterTest
	public void tearDownTest() {
		// close browser
		driver.close();
		driver.quit();
		System.out.println("Test Compleated Successfully");
	}

}
