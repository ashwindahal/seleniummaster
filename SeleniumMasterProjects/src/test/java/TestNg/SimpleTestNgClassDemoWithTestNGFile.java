package TestNg;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SimpleTestNgClassDemoWithTestNGFile {

	static WebDriver driver;

	@BeforeTest
	public void setUpTest() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver();
	}

	public static void navigateToGoogle2() {
		driver.navigate().to("http://www.google.com");
		driver.navigate().refresh();
		System.out.println(driver.getTitle() + "----------------------------------------------------Was lauched");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test
	public static void performActions2() {
		// navigate to google.com
		navigateToGoogle2();

		// creating Webelemet Objects
		WebElement txtBoxSearch = driver.findElement(By.xpath("//input[@name='q']"));
		WebElement searchBtn = driver.findElement(By.xpath("//input[@name='btnK']"));

		// Utilizing the objects to perform actions
		txtBoxSearch.sendKeys("What is TestNg");
		searchBtn.sendKeys(Keys.RETURN);

	}

	@AfterTest
	public static void terminateTest() {
		driver.close();
		driver.quit();
	}
}
