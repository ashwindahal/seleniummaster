package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CerotidPageObjectsAndMethods {

	WebDriver driver = null;

	By selectCourse = By.xpath("//select[contains(@data-validation-required-message,'Please select Course.')]");
	By selectSession = By.xpath("//select[contains(@data-validation-required-message,'Please select Session Date.')]");
	By enterName = By.xpath("//input[contains(@id,'name')]");
	By sendAddress = By.xpath("//input[contains(@id,'address')]");
	By sendCity = By.xpath("//input[contains(@id,'city')]");
	By selectState = By.xpath("//select[contains(@id,'state')]");
	By enterZip = By.xpath("//input[contains(@id,'zip')]");
	By enterEmail = By.xpath("//input[contains(@id,'email')]");
	By enterPhone = By.xpath("//input[contains(@id,'phone')]");
	By chooseVisa = By.xpath("//select[contains(@id,'visaStatus')]");
	By chooseMediaSource = By.xpath("//select[contains(@id,'media')]");
	By relocateRadioBtn = By.xpath("//input[@id='relocate' and @value='Yes']");
	By educationBGForm = By.xpath("//textarea[@id='eduDetails']");
	By submitBtn = By.xpath("//button[@id='registerButton']");
	By validateSuccessMessage = By
			.xpath("//strong[contains(text(),'Your register is completed. We will contact you shortly!')]");

	public CerotidPageObjectsAndMethods(WebDriver driver) {
		this.driver = driver;
	}

	public void selectCourse(String courseName) {
		// Creating new webelemt obj and passing select course as a By parameter
		WebElement element = driver.findElement(selectCourse);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(courseName);
	}

	public void selectSession(String sessionName) {
		// Creating new webelemt obj and passing select course as a By parameter
		WebElement element = driver.findElement(selectSession);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(sessionName);
	}

	public void enterName(String nameValue) {
		driver.findElement(enterName).sendKeys(nameValue);
	}

	public void enterAddress(String enterAddress) {
		driver.findElement(sendAddress).sendKeys(enterAddress);
	}

	public void enterCity(String enterCity) {
		driver.findElement(sendCity).sendKeys(enterCity);
	}

	public void selectState(String state) {
		WebElement element = driver.findElement(selectState);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(state);
	}

	public void enterZip(String zip) {
		driver.findElement(enterZip).sendKeys(zip);
	}

	public void enterEmail(String email) {
		driver.findElement(enterEmail).sendKeys(email);
	}

	public void enterPhoneNumber(String phoneNumber) {
		driver.findElement(enterPhone).sendKeys(phoneNumber);
	}

	public void chooseVisa(String visa) {
		WebElement element = driver.findElement(chooseVisa);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(visa);
	}

	public void chooseMediaSource(String mediaSource) {
		WebElement element = driver.findElement(chooseMediaSource);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(mediaSource);
	}

	public void relocateBtn() {
		driver.findElement(relocateRadioBtn).click();
	}

	public void eduBgForm(String formvalue) {
		driver.findElement(educationBGForm).sendKeys(formvalue);
	}

	public void clickOnSubmit() {
		driver.findElement(submitBtn).click();
	}

	public String validateMessage() {
		String message = driver.findElement(validateSuccessMessage).getText();
		return message;
	}

	public boolean isElementPresent(By by) {
		boolean check = false;
		try {
			driver.findElement(by);
			check = true;

		} catch (Exception e) {
			e.printStackTrace();
			check = false;
		}
		return check;
	}
}
