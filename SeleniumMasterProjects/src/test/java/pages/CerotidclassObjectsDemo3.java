package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CerotidclassObjectsDemo3 {

	WebDriver driver = null;
	WebElement element = null;

	By selectCourse = By.xpath("//select[contains(@data-validation-required-message,'Please select Course.')]");
	By selectSession = By.xpath("//select[contains(@data-validation-required-message,'Please select Session Date.')]");
	By enterName = By.xpath("//input[contains(@id,'name')]");
	By sendAddress = By.xpath("//input[contains(@id,'address')]");
	By sendCity = By.xpath("//input[contains(@id,'city')]");
	By selectState = By.xpath("//select[contains(@id,'state')]");
	By enterZip = By.xpath("//input[contains(@id,'zip')]");
	By enterEmail = By.xpath("//input[contains(@id,'email')]");
	By enterPhone = By.xpath("//input[contains(@id,'phone')]");
	By chooseVisa = By.xpath("//select[contains(@id,'visaStatus')]");
	By chooseMediaSource = By.xpath("//select[contains(@id,'media')]");
	By relocateRadioBtn = By.xpath("//input[@id='relocate' and @value='Yes']");
	By educationBGForm = By.xpath("//textarea[@id='eduDetails']");
	By submitBtn = By.xpath("//button[@id='registerButton']");
	By validateSuccessMessage = By
			.xpath("//strong[contains(text(),'Your register is completed. We will contact you shortly!')]");

	public CerotidclassObjectsDemo3(WebDriver driver) {
		this.driver = driver;
	}

	public void selectCourse(String courseName) {
		element = driver.findElement(selectCourse);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(courseName);
	}

	public void selectSession(String sessionName) {
		element = driver.findElement(selectSession);
		Select chooseSession = new Select(element);
		chooseSession.selectByVisibleText(sessionName);
	}

	public void enterName(String fullName) {
		driver.findElement(enterName).sendKeys(fullName);
	}
	
	public void sendAddress(String address) {
		driver.findElement(sendAddress).sendKeys(address);
	}
	
	public void enterCity(String city) {
		driver.findElement(sendCity).sendKeys(city);
	}
}
