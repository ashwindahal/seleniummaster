package config;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.CerotidPageObjectsAndMethods;

public class PropertiesConfigurationWithTestNG {

	// Global Variables
	WebDriver driver;
	// 1 Configuration browserName have to set to public so it can be accessed by
	// the PropertiesFile class
	public static String browserName = null;

	@BeforeTest
	public void setUpTest() {
		// Reads configuration file and invokes driver based on the configuration
		// Creating a obj of Properties to get the properties currently configured
		PropertiesFile.getProperties();

		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", ".\\libs\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
	}

	public void navigateToWebPage() {
		driver.get("http://www.cerotid.com");
		if (driver.getTitle().contains("Cerotid")) {
			System.out.println(driver.getTitle() + "----------------------------------------------------Was lauched");
		} else {
			System.out.println("Failed--------------------Incorrect title page");
			System.exit(0);
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Test(priority = 0)
	public void CerotidHomePageTest1() throws Exception {
		// Navigate to cerotid page
		navigateToWebPage();

		// Creating Page object
		CerotidPageObjectsAndMethods cerotidPageObj = new CerotidPageObjectsAndMethods(driver);
		// Utlizing Cerotid page objects and performing actions
		// Sending Validations to Extent reports
		cerotidPageObj.selectCourse("QA Automation");
		cerotidPageObj.selectSession("Upcoming Session");
		cerotidPageObj.enterName("Test Tester");
		cerotidPageObj.enterAddress("2300 Valley View Ln Suite 620, Irving, TX 75062");
		cerotidPageObj.enterCity("Irving");
		cerotidPageObj.selectState("TX");
		cerotidPageObj.enterZip("75062");
		cerotidPageObj.enterEmail("com.cerotid.qa.test@cerotid.com");
		cerotidPageObj.enterPhoneNumber("(972)255-0046");

		System.out.println("Compleated Test 1");

	}

	@Test(priority = 1)
	public void CerotidHomePageTest2() {
		CerotidPageObjectsAndMethods cerotidPageObj = new CerotidPageObjectsAndMethods(driver);
		cerotidPageObj.chooseVisa("Other");
		cerotidPageObj.chooseMediaSource("Social Media");
		cerotidPageObj.relocateBtn();
		cerotidPageObj.eduBgForm("Currently Learning at Cerotid");

		System.out.println("Compleated Test 2");

	}

	@AfterTest
	public void tearDownTestCloseBrowser() {
		System.out.println("Terminated the browser");
		driver.close();
		//Quit has issues with firefox
		//driver.quit();
		
		//Setting the Properties to pass if Script gets to this point 
		PropertiesFile.setProperties();
	}

}
