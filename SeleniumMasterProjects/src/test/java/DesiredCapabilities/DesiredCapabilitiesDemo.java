package DesiredCapabilities;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DesiredCapabilitiesDemo {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		// DesiredCapabilities caps = new DesiredCapabilities();
		// caps.setCapability("ignoreProtectedModeSettings", true);
		// caps.setVersion("79.0.3945.36");
		// caps.chrome().setVersion("79.0.3945.36");

		// Incognito mode
		/*
		 * ChromeOptions options = new ChromeOptions();
		 * options.addArguments("--incognito"); DesiredCapabilities capabilities = new
		 * DesiredCapabilities(); capabilities.setCapability(ChromeOptions.CAPABILITY,
		 * options); options.merge(capabilities);
		 * 
		 * WebDriver driver = new ChromeDriver(options);
		 */

		
		//Head less Demo (Browser is not invoked program runs in background)
		/*
		 * ChromeOptions options = new ChromeOptions();
		 * options.addArguments("--headless"); DesiredCapabilities capabilities = new
		 * DesiredCapabilities(); capabilities.setCapability(ChromeOptions.CAPABILITY,
		 * options); options.merge(capabilities);
		 * 
		 * WebDriver driver = new ChromeDriver(options);
		 * 
		 * driver.get("http://Google.com");
		 * driver.findElement(By.name("q")).sendKeys("ABCD");
		 * driver.findElement(By.name("btnK")).sendKeys(Keys.RETURN);
		 * 
		 * String title = driver.getTitle(); System.out.println("Page Title: " +title);
		 * 
		 * driver.close(); driver.quit();
		 */
	
		
		//Ad Blocker 
		ChromeOptions options = new ChromeOptions();;
		
		options.addExtensions(new File(".\\libs\\extension_4_5_0_0.crx")); 
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		options.merge(capabilities);

		WebDriver driver = new ChromeDriver(options);

		driver.get("https://www.guru99.com/smoke-testing.html#7");
		//driver.close();
		//driver.quit();

	}

}
