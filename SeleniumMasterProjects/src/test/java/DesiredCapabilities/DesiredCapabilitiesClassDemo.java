package DesiredCapabilities;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DesiredCapabilitiesClassDemo {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// incognoto
		/*
		 * //Incognito Exmaple ChromeOptions options = new ChromeOptions();
		 * options.addArguments("--incognito"); DesiredCapabilities capabilities = new
		 * DesiredCapabilities(); capabilities.setCapability(ChromeOptions.CAPABILITY,
		 * options); options.merge(capabilities);
		 */

		// Headless
		/*
		 * ChromeOptions options = new ChromeOptions();
		 * options.addArguments("--headless"); DesiredCapabilities capabalities = new
		 * DesiredCapabilities(); capabalities.setCapability(ChromeOptions.CAPABILITY,
		 * options); options.merge(capabalities);
		 * 
		 * WebDriver driver = new ChromeDriver(options); driver.get("http:google.com");
		 * driver.findElement(By.name("q")).sendKeys("DesiredCapabilities");
		 * driver.findElement(By.name("btnK")).sendKeys(Keys.RETURN); String title =
		 * driver.getTitle(); System.out.println("Page Title: " + title); driver.quit();
		 */

		// Add Blocker
		ChromeOptions options = new ChromeOptions();
		options.addExtensions(new File(".\\libs\\extension_4_5_0_0.crx"));
		DesiredCapabilities capabilities = new DesiredCapabilities(); 
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		options.merge(capabilities);
		
		WebDriver driver = new ChromeDriver(options);
		driver.get("https://www.guru99.com/smoke-testing.html#7");
		
		
		
	}
}
