package utils;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtilsSimple {

	public static void main(String[] args) {
		getRowCount();
		getCellDataString();
		getCellDataNumericValue();
	}

	public static void getRowCount() {

		try {
			// 1: Create Excel file and add some data to it
			// 2: Creating Refrence for Workbook and providing the location of the workbook
			XSSFWorkbook workbook = new XSSFWorkbook(".\\excel\\data.xlsx");
			// 3: Create references for worksheet from the excel file
			XSSFSheet sheet = workbook.getSheet("Sheet1");
			// 4: Call row count function and store in int value
			int rowCount = sheet.getPhysicalNumberOfRows();
			// Printing Row count
			System.out.println("Number of rows : " + rowCount);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}
	}

	public static void getCellDataString() {

		try {
			// 1: Create Excel file and add some data to it
			// 2: Creating Refrence for Workbook and providing the location of the workbook
			XSSFWorkbook workbook = new XSSFWorkbook(".\\excel\\data.xlsx");
			// 3: Create references for worksheet from the excel file
			XSSFSheet sheet = workbook.getSheet("Sheet1");

			// Call Function to get cell data of type string--> specify the row and column
			// number based on
			// excel sheet
			String cellData = sheet.getRow(0).getCell(0).getStringCellValue();
			// Printing the Cell data
			System.out.println(cellData);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}

	}

	public static void getCellDataNumericValue() {

		try {
			// 1: Create Excel file and add some data to it
			// 2: Creating Refrence for Workbook and providing the location of the workbook
			XSSFWorkbook workbook = new XSSFWorkbook(".\\excel\\data.xlsx");
			// 3: Create references for worksheet from the excel file
			XSSFSheet sheet = workbook.getSheet("Sheet1");

			// Call Function to get numeric cell data specifiy the row and column number
			// based on
			// excel sheet
			double cellData = sheet.getRow(1).getCell(1).getNumericCellValue();
			// Printing the numeric Cell data
			System.out.println(cellData);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}

	}

}
