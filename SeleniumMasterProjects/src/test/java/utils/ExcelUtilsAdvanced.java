package utils;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtilsAdvanced {

	// ----CLASS LEVEL VARIBLES----
	static XSSFWorkbook workbook;
	static XSSFSheet sheet;

	// Creating constructor to utilize ExcelUtilsAdvanced class
	public ExcelUtilsAdvanced(String excelPath, String sheetName) {
		try {
			workbook = new XSSFWorkbook(excelPath);
			sheet = workbook.getSheet(sheetName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getRowCount() {

		try {
			// 1: Create Excel file and add some data to it
			// 2: Creating Refrence for Workbook and providing the location of the workbook
			// workbook = new XSSFWorkbook(".\\excel\\data.xlsx");
			// 3: Create references for worksheet from the excel file
			// sheet = workbook.getSheet("Sheet1");
			// 4: Call row count function and store in int value
			int rowCount = sheet.getPhysicalNumberOfRows();
			// Printing Row count
			System.out.println("Number of rows : " + rowCount);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}
	}

	public void getCellDataString(int rowNum, int colNum) {

		try {
			// 1: Create Excel file and add some data to it
			// 2: Creating Refrence for Workbook and providing the location of the workbook
			// workbook = new XSSFWorkbook(".\\excel\\data.xlsx");
			// 3: Create references for worksheet from the excel file
			// sheet = workbook.getSheet("Sheet1");

			// Call Function to get cell data of type string--> specify the row and column
			// number based on
			// excel sheet
			String cellData = sheet.getRow(rowNum).getCell(colNum).getStringCellValue();
			// Printing the Cell data
			System.out.println(cellData);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}

	}

	public void getCellDataNumericValue(int rowNum, int colNum) {

		try {
			// 1: Create Excel file and add some data to it
			// 2: Creating Refrence for Workbook and providing the location of the workbook
			// workbook = new XSSFWorkbook(".\\excel\\data.xlsx");
			// 3: Create references for worksheet from the excel file
			// sheet = workbook.getSheet("Sheet1");

			// Call Function to get numeric cell data specifiy the row and column number
			// based on
			// excel sheet
			double cellData = sheet.getRow(rowNum).getCell(colNum).getNumericCellValue();
			// Printing the numeric Cell data
			System.out.println(cellData);

		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		}

	}

}
