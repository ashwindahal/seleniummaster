package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.CerotidclassObjectsDemo3;

public class CerotidObjectsDemo3test {

	public static WebDriver driver = null;

	public static void main(String[] args) {
		// Step 1: Invoke Browser
		invokeBrowser();
		// Step 2: Performing actions on the web page
		cerotidSignUpFlow();
		// Step 3: Close the Browser
		closeBrowser();

	}

	public static void invokeBrowser() {
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		driver = new ChromeDriver(); 
		driver.navigate().to("http://www.cerotid.com");
		System.out.println(driver.getTitle() + "----------------------------------------------------Was lauched");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	public static void cerotidSignUpFlow() {
		//Creating a Page Object 
		CerotidclassObjectsDemo3 obj = new CerotidclassObjectsDemo3(driver); 
		obj.selectCourse("Java");
		obj.selectSession("Upcoming Session");
		obj.enterName("Test tester");
	}

	public static void closeBrowser() {
		// Close the browser
//		driver.close();
//		driver.quit();

	}

}
