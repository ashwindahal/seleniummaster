package test;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.CerotidPageObjectsAndMethods;

public class GoogleTest {

	public static void main(String[] args) throws InterruptedException {
		// Creating webdriver obj
		// setting system path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		// creating new chromedriver obj
		WebDriver driver = new ChromeDriver();
		//invoking the web browser 
		driver.get("http://www.google.com");
		//maximize the browser
		driver.manage().window().maximize();
		/*
		 * //Created a webElement obj for the txtBoxsearch WebElement txtBoxSearch =
		 * driver.findElement(By.xpath("//input[@name='q']"));
		 * txtBoxSearch.sendKeys("What is selenium");
		 */
		
		CerotidPageObjectsAndMethods obj = new CerotidPageObjectsAndMethods(driver);
		if(obj.isElementPresent(By.xpath("//input[@name='q']"))) {
			WebElement txtBoxSearch = driver.findElement(By.xpath("//input[@name='q']"));
			txtBoxSearch.sendKeys("What is selenium");
		}else {
			System.out.println("");
		}
		
		WebElement searchBtn = driver.findElement(By.xpath("//input[@name='btnK']")); 
		searchBtn.sendKeys(Keys.RETURN); 
		
		//Implicity waits
		TimeUnit.SECONDS.sleep(5);
		
		//explicity
		WebDriverWait waite = new WebDriverWait(driver, 5);
		waite.wait(5);

		
		
		
		
	
		
		
		
		
	}
}
